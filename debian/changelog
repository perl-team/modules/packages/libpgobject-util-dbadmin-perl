libpgobject-util-dbadmin-perl (1.6.2-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.6.2.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 16 Sep 2024 19:20:36 +0200

libpgobject-util-dbadmin-perl (1.6.1-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.6.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Nov 2021 16:15:15 +0100

libpgobject-util-dbadmin-perl (1.5.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 1.5.0.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Tue, 28 Sep 2021 19:20:53 +0200

libpgobject-util-dbadmin-perl (1.4.0-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.4.0.
  * Add new test/runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Wed, 28 Oct 2020 18:59:02 +0100

libpgobject-util-dbadmin-perl (1.2.2-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.2.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Oct 2020 00:46:01 +0200

libpgobject-util-dbadmin-perl (1.1.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 1.1.0.
  * Update years of upstream and packaging copyright.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Sep 2020 15:35:06 +0200

libpgobject-util-dbadmin-perl (1.0.3-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.0.3.
  * Update years of upstream and packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Set upstream metadata fields: Bug-Database.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2019 21:51:26 +0100

libpgobject-util-dbadmin-perl (0.130.1-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Update years of upstream and packaging copyright.
  * Don't run new perlcritic test during build and autopkgtest.
  * Add (build) dependency on libnamespace-clean-perl.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

  [ Robert James Clay ]
  * Update my copyright years in debian/copyright.
  * Import upstream version 0.130.1, resolving CVE-2018-9246. (Closes: #900942)
  * Correct the upstream URL metadata in debian/upstream/metadata.
  * Add 't/boilerplate.t' to the debian/tests/pkg-perl/smoke-skip file.

 -- Robert James Clay <jame@rocasa.us>  Thu, 07 Jun 2018 10:55:23 -0400

libpgobject-util-dbadmin-perl (0.100.0-1) unstable; urgency=low

  * Initial Release. (Closes: #824214)

 -- Robert James Clay <jame@rocasa.us>  Sun, 18 Dec 2016 02:27:58 -0500
